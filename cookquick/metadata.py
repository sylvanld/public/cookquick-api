"""
Metadata that describes app and its version
"""

TITLE = "CookQuick API"
DESCRIPTION = "API that enables storage / search of recipes, building shopping list, and shopping together"
VERSION = "0.0.1"
