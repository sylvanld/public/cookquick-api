FROM python:3.8-alpine

WORKDIR /cookquick

RUN apk add --no-cache gcc g++ musl-dev

COPY requirements/prod.txt /tmp/requirements
RUN pip install --no-cache-dir -r /tmp/requirements

COPY setup.py .
COPY cookquick cookquick/

CMD [ "uvicorn", "--factory", "cookquick.server:CookQuickAPI", "--host", "0.0.0.0", "--port", "80" ]
